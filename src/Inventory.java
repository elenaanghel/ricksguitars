import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Inventory {

    private List<Guitar> guitars;

    public Inventory() {
        this.guitars = new LinkedList<>();
    }

    public void addGuitar(String serialNumber, double price, String builder, String model, String type, String backWood, String topWood) {
        Guitar guitar = new Guitar(serialNumber, price, builder, model, type, backWood, topWood);
        guitars.add(guitar);
    }

    public Guitar getGuitar(String serialNumber){
        for (Iterator<Guitar> iterator = guitars.iterator(); iterator.hasNext();){
           Guitar guitar= iterator.next();
           if(guitar.getSerialNumber().equals(serialNumber)){
               return guitar;
           }
        }
        return null;
    }

    public Guitar search(Guitar searchGuitar){
        for(Iterator<Guitar> iterator = guitars.iterator(); iterator.hasNext();){
            Guitar guitar = iterator.next();

        }
        return null;
    }
}
